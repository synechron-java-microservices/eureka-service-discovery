# Spring Cloud Eureka Server 
## Setting up Eureka Discovery Server 

1. Set up the Spring Boot project with the following configuration from [start.spring.io](https://start.spring.io) portal
   - Spring Version - 2.5.3
   - Java Version - 8
   - Spring Cloud Version - 2020.0.3

2. Add the following dependencies 
```xml
     <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
     </dependency>
```

3. Create a file called `bootstrap.yaml` under `src/main/resources` directory

4. Add the following code under the `bootstrap.yaml` file 
```yaml
spring:
  application:
    name: eureka-server
```
5. In the `application.yaml` file add the below code 
```yaml
server:
  port: 8761
eureka:
  server:
    enable-self-preservation: false
  instance:
    lease-expiration-duration-in-seconds: 90
    lease-renewal-interval-in-seconds: 30
    hostname: localhost
  client:
    register-with-eureka: false
    fetch-registry: false
    serviceUrl:
      defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka
```
6. Add the `@EnableEurekaServer` annotation in the Root file

9. Verify the url `http://localhost:8761` and you can view the Eureka Dashboard
